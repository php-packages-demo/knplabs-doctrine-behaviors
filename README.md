# knplabs-doctrine-behaviors

Doctrine2 behavior traits. https://packagist.org/packages/knplabs/doctrine-behaviors

[![PHPPackages Rank](http://phppackages.org/p/knplabs/doctrine-behaviors/badge/rank.svg)](http://phppackages.org/p/knplabs/doctrine-behaviors)
[![PHPPackages Referenced By](http://phppackages.org/p/knplabs/doctrine-behaviors/badge/referenced-by.svg)](http://phppackages.org/p/knplabs/doctrine-behaviors)

## Unofficial documentation
### Internationalization
* [*Your Guide to Symfony 4 Internationalization Beyond the Basics*
  ](https://medium.com/@phrase/your-guide-to-symfony-4-internationalization-beyond-the-basics-122c6a708c8)
  2019-09 Phrase
